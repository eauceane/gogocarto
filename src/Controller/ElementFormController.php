<?php

/**
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright Copyright (c) 2016 Sebastian Castro - 90scastro@gmail.com
 * @license    MIT License
 * @Last Modified time: 2018-07-08 16:44:57
 */

namespace App\Controller;

use App\Document\Element;
use App\Document\ElementStatus;
use App\Form\ElementType;
use App\Services\ConfigurationService;
use App\Services\ElementFormService;
use App\Services\ElementSynchronizationService;
use Doctrine\ODM\MongoDB\DocumentManager;
use FOS\UserBundle\Model\UserManagerInterface;
use FOS\UserBundle\Security\LoginManagerInterface;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Contracts\Translation\TranslatorInterface;


class ElementFormController extends GoGoController
{
    public function __construct(ElementSynchronizationService $synchService)
    {
        $this->synchService = $synchService;
    }
    
    public function addAction(Request $request, SessionInterface $session, DocumentManager $dm,
                              ConfigurationService $configService,
                              ElementFormService $elementFormService, UserManagerInterface $userManager,
                              LoginManagerInterface $loginManager, 
                              TranslatorInterface $t)
    {
        return $this->renderForm(new Element(), false, $request, $session, $dm, $configService, $elementFormService, $userManager, $loginManager, $t);
    }

    public function editAction($id, Request $request, SessionInterface $session, DocumentManager $dm,
                               ConfigurationService $configService,
                               ElementFormService $elementFormService, UserManagerInterface $userManager,
                               LoginManagerInterface $loginManager, 
                               TranslatorInterface $t)
    {
        $element = $dm->get('Element')->find($id);

        if (!$element) {
            $this->addFlash('error', $t->trans('element.form.controller.error_dont_exist'));

            return $this->redirectToRoute('gogo_directory');
        } elseif ($element->getStatus() > ElementStatus::PendingAdd && $element->isEditable()
            || $configService->isUserAllowed('directModeration')
            || ($element->isPending() && $element->getRandomHash() == $request->get('hash'))) {
            return $this->renderForm($element, true, $request, $session, $dm, $configService, $elementFormService, $userManager, $loginManager, $t);
        } else {
            $this->addFlash('error', $t->trans('element.form.controller.error_unauthorized'));

            return $this->redirectToRoute('gogo_directory');
        }
    }

    // render for both Add and Edit actions
    private function renderForm($element, $editMode, $request, $session, $dm, $configService,
                                $elementFormService, $userManager, $loginManager, TranslatorInterface $t)
    {
        if (null === $element) {
            throw new NotFoundHttpException($t->trans('element.form.controller.error_dont_exist'));
        }

        if ($request->get('logout')) {
            $session->remove('emailToCreateAccount');
        }

        $isEditingWithHash = $element->getRandomHash() && $element->getRandomHash() == $request->get('hash');
        $isLoggedIn = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED');
        
        // Create user uppon element form submission (first we ask email, then password inside the element form)
        if ($request->request->get('input-password') && !$isLoggedIn) {
            // Create user and set details
            $user = $userManager->createUser();
            $user->setUserName($session->get('emailToCreateAccount'));
            $user->setEmail($session->get('emailToCreateAccount'));
            $user->setPlainPassword($request->request->get('input-password'));
            $user->setEnabled(true);
            $userManager->updateUser($user, true);
            $dm->persist($user);
            // Authenticate User
            $loginManager->loginUser($this->getParameter('fos_user.firewall_name'),$user, null);
            $isLoggedIn = true;
            // Add flash message
            $text = $t->trans('element.form.controller.success', ['%url%' => $this->generateUrl('gogo_user_profile')] );
            $session->getFlashBag()->add('success', $text);
        }
        
        // is user not allowed, we show the contributor-login page
        $featureName = $editMode ? 'edit' : 'add';
        if (!$configService->isUserAllowed($featureName, $request) && !$isEditingWithHash) {
            // creating simple form to let user enter an email address
            $loginform = $this->get('form.factory')->createNamedBuilder('user', FormType::class)
                ->add('email', EmailType::class, ['required' => false])
                ->getForm();
            $loginEmail = $request->request->get('user')['email'] ?? '';
            $emailAlreadyUsed = false;
            if ($loginEmail) {
                $othersUsers = $dm->get('User')->findByEmail($loginEmail);
                $emailAlreadyUsed = count($othersUsers) > 0;
            }
            $loginform->handleRequest($request);
            if ($loginform->isSubmitted() && $loginform->isValid() && !$emailAlreadyUsed) {
                $session->set('emailToCreateAccount', $loginEmail);
            } else {
                return $this->render('element-form/contributor-login.html.twig', [
                    'loginForm' => $loginform->createView(),
                    'emailAlreadyUsed' => $emailAlreadyUsed,
                    'config' => $configService->getConfig(),
                    'featureConfig' => $configService->getFeatureConfig($featureName), ]);
            }
        }

        $userEmail = $isLoggedIn ? $this->getUser()->getEmail() : $session->get('emailToCreateAccount') ?? '';

        // We need to detect if the owner contribution has been validated. Because after that, the owner have direct moderation on the element
        // To check that, we check is element is Valid or element is pending but from a contribution not made by the owner
        $isUserOwnerOfValidElement = 
            $isLoggedIn && $editMode
            && ($element->isValid() || $element->isPending() && $element->getCurrContribution() && $element->getCurrContribution()->getUserEmail() != $userEmail)
            && $element->getUserOwnerEmail() && $element->getUserOwnerEmail() == $userEmail;
        
        $isAllowedDirectModeration = 
            $configService->isUserAllowed('directModeration')
            || !$editMode && $isLoggedIn && $this->getUser()->hasRole('ROLE_DIRECTMODERATION_ADD')
            || $editMode && $isLoggedIn && $this->getUser()->hasRole('ROLE_DIRECTMODERATION_EDIT_OWN_CONTRIB') && $element->hasValidContributionMadeBy($userEmail)
            || $isUserOwnerOfValidElement
            || $isEditingWithHash && !$element->isPending();

        // when we check for duplicates, we jump to an other action, and come back to this action
        // with the "checkDuplicateDone" GET param to true
        $checkDuplicateDone = $request->query->get('checkDuplicateDone') && $session->has('pendingRequest');
        if ($session->get('pendingRequest')) $request = $session->get('pendingRequest');

        $originalElement = $element->clone(); // will be use to compute changeset
        
        // create the element form
        $elementForm = $this->get('form.factory')->create(ElementType::class, $element);
        $elementForm->handleRequest($request);

        // If form submitted with valid values
        if ($elementForm->isSubmitted() && $elementForm->isValid() || $checkDuplicateDone) {
            // custom handling form (OptionValues, custom data...)
            $element = $elementFormService->handleFormSubmission($element, $request, $userEmail);
            // if checkDuplicate process is done
            if ($checkDuplicateDone) {                
                // handle match with OSM
                if ($osmId = $request->get('matchElementWithOsmId')) {
                    $this->synchService->updateOsmDuplicateWithNewElementData($osmId, $element);
                }
            }
            elseif (!$editMode) {               
                // check for duplicates
                $duplicates = $dm->get('Element')->findDuplicatesFor($element);
                $osmDuplicates = $this->synchService->checkIfNewElementShouldBeAddedToOsm($element)['duplicates'];
                if (count($osmDuplicates)) {
                    // the duplicates returnes by OSM might already exist in gogocarto, so we
                    // intersect osmduplicates with gogo duplicates
                    $osmDuplicatesIdsInGoGoDatabase = [];
                    foreach($duplicates as $duplicate) {
                        if ($duplicate->isFromOsm()) $osmDuplicatesIdsInGoGoDatabase[] = $duplicate->getOldId();
                    }
                    foreach($osmDuplicates as $osmDuplicate) {
                        $id = explode('/', $osmDuplicate['osmId'])[1]; // osmId is something like node/12345
                        if (!in_array($id, $osmDuplicatesIdsInGoGoDatabase))
                            $duplicates[] = $osmDuplicate;
                    }
                }
                if (count($duplicates) > 0) {
                    // saving values in session
                    $session->set('pendingRequest', $request);
                    $session->set('duplicatesElements', $duplicates);
                    // redirect to check duplicate
                    return $this->redirectToRoute('gogo_element_check_duplicate');
                }
            }
            
            // If we land here it means there is no duplicates or duplicates process is done, so save element
            $element = $elementFormService->save($element, $originalElement, $request, $isAllowedDirectModeration);
            $dm->persist($element);
            $dm->flush();

            // Send add email (now the element is persisted)
            if (!$originalElement->getId()) $elementFormService->afterAdd($element, $request);

            // clear session
            $session->remove('pendingRequest');
            $session->remove('duplicatesElements');

            // Create flash message
            $elementShowOnMapUrl = $element->getShowUrlFromController($this->get('router'));
            $recopyInfo = $request->request->get('recopyInfo');
            $submitOption = $request->request->get('submit-option');
            if ($editMode) {
                $noticeText = $t->trans('element.form.controller.thankyou.edited');
            } else {
                $noticeText = $t->trans('element.form.controller.thankyou.added', ['%name%' => ucwords($configService->getConfig()->getElementDisplayNameDefinite())]);
            }
            if ($element->isPending()) {
                $noticeText .= "<br/>".$t->trans('element.form.controller.pending');
                if ($configService->getFeatureConfig('vote')->getActive())
                    $noticeText .= <<<HTML
                        , <a class='validation-process' onclick="$('#popup-collaborative-explanation').openModal()">
                            {$t->trans('commons.click_here')}
                        </a> {$t->trans('element.form.controller.pending_collaborative')}
                    HTML;
            }
            if ($isLoggedIn) {
                $noticeText .= '<br/>'.$t->trans('element.form.controller.user_contributions', ['%url%' => $this->generateUrl('gogo_user_contributions')]);
            }
            if ($submitOption == 'stayonform' ) {
                $noticeText .= '<br/><a href="'.$elementShowOnMapUrl.'">Voir le résultat sur la carte</a>';
            }
            $session->getFlashBag()->add('success', $noticeText);

            // Render a new form a redirect to map
            if ($submitOption == 'stayonform' || $recopyInfo) {
                if (!$recopyInfo) $element = new Element();
                $elementForm = $this->get('form.factory')->create(ElementType::class, $element);
                $editMode = false;
            } else {
                return $this->redirect($elementShowOnMapUrl);
            }
        } // ends  handling submitted form

        // If pending modif, we update the modifiedElement (user editing it's own pending contrib)
        $elementToFillTheForm = $element->isPendingModification() ? $element->getModifiedElement() : $element;
        
        return $this->render('element-form/element-form.html.twig', [
            'editMode' => $editMode,
            'form' => $elementForm->createView(),
            'mainCategories' => $dm->get('Category')->findRootCategories(),
            'element' => $elementToFillTheForm,
            'isAllowedDirectModeration' => $isAllowedDirectModeration,
            'config' => $configService->getConfig(),
            'imagesMaxFilesize' => $this->detectMaxUploadFileSize('images'),
            'filesMaxFilesize' => $this->detectMaxUploadFileSize('files'),
            'isOwner' => $isUserOwnerOfValidElement
        ]);
    }

    // when submitting new element, check it's not yet existing
    public function checkDuplicatesAction(Request $request, SessionInterface $session, DocumentManager $dm)
    {
        // a form with just a submit button
        $checkDuplicatesForm = $this->get('form.factory')->createNamedBuilder('duplicates', FormType::class)
                                                         ->getForm();        
    
        // If Form is Submitted
        if ($request->getMethod() == 'POST') {
            // if user say that it's not a duplicate, we go back to add action with checkDuplicateDone to true
            $osmId = $request->get('osm') ? array_keys($request->get('osm'))[0] : null;            
            return $this->redirectToRoute('gogo_element_add', ['checkDuplicateDone' => true, 'matchElementWithOsmId' => $osmId]);
        }        
        elseif ($session->has('duplicatesElements') && count($session->get('duplicatesElements')) > 0) {
            // Display the check duplicates form
            $duplicates = $session->get('duplicatesElements');
            $config = $dm->get('Configuration')->findConfiguration();
            return $this->render('element-form/check-for-duplicates.html.twig', [
                'duplicateForm' => $checkDuplicatesForm->createView(),
                'duplicatesElements' => $duplicates,
                'config' => $config ]);
        }        
        else {
            return $this->redirectToRoute('gogo_element_add');
        }
    }

    /**
     * Detects max size of file cab be uploaded to server.
     *
     * Based on php.ini parameters "upload_max_filesize", "post_max_size" &
     * "memory_limit". Valid for single file upload form. May be used
     * as MAX_FILE_SIZE hidden input or to inform user about max allowed file size.
     *
     * @return int Max file size in bytes
     */
    private function detectMaxUploadFileSize($key = null)
    {
        /**
         * Converts shorthands like "2M" or "512K" to bytes.
         *
         * @param $size
         *
         * @return mixed
         */
        $normalize = function ($size) {
            if (preg_match('/^([\d\.]+)([KMG])$/i', $size, $match)) {
                $pos = array_search($match[2], ['K', 'M', 'G']);
                if (false !== $pos) {
                    $size = $match[1] * pow(1024, $pos + 1);
                }
            }

            return $size;
        };

        $max_upload = $normalize(ini_get('upload_max_filesize'));
        $max_post = $normalize(ini_get('post_max_size'));
        $memory_limit = $normalize(ini_get('memory_limit'));
        $maxFileSize = min($max_upload, $max_post, $memory_limit);

        if ($key) {
            $appMaxsize = $this->getParameter($key.'_max_filesize');
            $maxFileSize = min($maxFileSize, $normalize($appMaxsize));
        }

        return $maxFileSize;
    }
}
