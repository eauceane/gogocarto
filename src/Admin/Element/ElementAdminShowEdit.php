<?php
/**
 * @Author: Sebastian Castro
 * @Date:   2017-03-28 15:29:03
 * @Last Modified by:   Sebastian Castro
 * @Last Modified time: 2018-06-09 14:29:33
 */

namespace App\Admin\Element;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use App\Form\ElementImageType;
use App\Form\ElementFileType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use App\Helper\GoGoHelper;
use Sonata\AdminBundle\Form\Type\ModelAutocompleteType;

class ElementAdminShowEdit extends ElementAdminList
{
    public $config;

    protected function configureFormFields(FormMapper $formMapper)
    {
        $dm = GoGoHelper::getDmFromAdmin($this);
        $this->config = $dm->get('Configuration')->findConfiguration();
        $categories = $dm->query('Option')->select('name')->getArray();
        $categoriesChoices = array_flip($categories);
        $elementProperties = $dm->get('Element')->findDataCustomProperties();
        $elementProperties = array_values(array_diff($elementProperties, array_keys($this->getSubject()->getData())));

        $aggregationMode = $this->config->getDuplicates()->getDuplicatesByAggregation();
        $isAggregate = $aggregationMode && $this->subject->isAggregate();
        $displayClass = $isAggregate ? "" : "hidden"; // this field should always been in the admin othewise autocomplete do not work, so just hide it when not needed
        $formMapper
          ->panel('aggregatedElements', ['box_class' => "box box-warning $displayClass"])
            ->add('aggregatedElements', ModelAutocompleteType::class, [
              'class' => 'App\Document\Element',
              'multiple' => true,
              'btn_add' => false,
              'property' => 'name',
              'label_attr'=> ['style'=> 'display:none'],
              'to_string_callback' => function($element, $property) {
                // hide agregates
                return $element->isAggregate() ? "" : "{$element->getName()} ({$element->getSourceKey()})";
              },
            ])
          ->end()
          ->halfPanel('general')
            ->add('name', null, [
              'required' => true,
              'disabled' => $isAggregate
            ])
            ->add('optionIds', ChoiceType::class, [
                'multiple' => true,
                'choices' => $categoriesChoices,
                'disabled' => $isAggregate,
              ], ['admin_code' => 'admin.options']
            )
            ->add('data', null, [
              'label_attr' => ['style' => 'display:none;'],
              'attr' => [
                'class' => 'gogo-element-data',
                'data-props' => json_encode($elementProperties)
              ]])
            ->add('userOwnerEmail', EmailType::class, [
              'disabled' => $isAggregate,  
            ])
            ->add('email', EmailType::class, [
              'disabled' => $isAggregate,  
            ])
            ->add('images', CollectionType::class, [
              'entry_type' => ElementImageType::class,
              'allow_add' => ! $isAggregate,
              'allow_delete' => ! $isAggregate,
            ])
            ->add('files', CollectionType::class, [
              'entry_type' => ElementFileType::class,
              'allow_add' => ! $isAggregate,
              'allow_delete' => ! $isAggregate,
            ])
            // ->add('openHours', OpenHoursType::class, ['required' => false])
          ->end()
          ->halfPanel('localisation')
            ->add('address.streetAddress', TextType::class, ['label_attr' => ['style' => 'display:none;'], 'attr' => ['class' => 'gogo-element-address']])
          ->end()
        ;       
    }

    protected function configureShowFields(ShowMapper $show)
    {
        if ($this->subject->getStatus() == -5) return; // modified pending version

        $show
          ->with('elements.form.groups.otherInfos', ['class' => 'col-md-6'])
            ->add('id')
            ->add('randomHash')
            ->add('oldId')
            ->add('sourceKey')
            ->add('createdAt', 'datetime', ['format' => $this->t('commons.date_time_format')])
            ->add('updatedAt', 'datetime', ['format' => $this->t('commons.date_time_format')])
          ->end();

        $show
          ->with('JSON', ['box_class' => 'box box-default'])
            ->add('compactJson')
            ->add('baseJson')
            ->add('adminJson')
          ->end();
    }
}