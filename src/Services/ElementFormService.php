<?php

namespace App\Services;

use App\Document\ElementStatus;
use App\Document\OptionValue;
use Doctrine\ODM\MongoDB\DocumentManager;
use App\Enum\UserInteractionType;

class ElementFormService
{
    /**
     * Constructor.
     */
    private $elementActionService;
    private $pendingService;
    private $dm;
    private $interactionService;

    public function __construct(ElementActionService $elementActionService, 
                                ElementPendingService $pendingService, DocumentManager $dm,
                                UserInteractionService $interactionService)
    {
        $this->elementActionService = $elementActionService;
        $this->pendingService = $pendingService;
        $this->dm = $dm;
        $this->interactionService = $interactionService;
    }

    // Update element with no standard form attributes (option values, custom data...)
    public function handleFormSubmission($element, $request, $userEmail)
    {
        $request = $request->request;
        $this->updateOptionsValues($element, $request);
        $this->updateCustomData($element, $request);
        $this->updateOwner($element, $request, $userEmail);
        return $element;
    }

    public function save($element, $originalElement, $request, $isAllowedDirectModeration)
    {
        $sendMail = $request ? $request->request->get('send_mail') : false;
        $message = $request ? $request->request->get('admin-message') : null;
        
        // in case user edit it's own contribution, the element is still pending, and
        // we want to make it pending again. So we delete previous contribution and create a new one
        if ($originalElement->isPending() && $currContrib = $originalElement->getCurrContribution()) {
            $originalElement->removeContribution($currContrib);
            $this->dm->remove($currContrib);
        }       

        if ($element->getId() && !$originalElement->isPendingAdd()) {
            // Edit
            if ($isAllowedDirectModeration || $this->isMinorModification($element, $originalElement)) {
                $this->elementActionService->edit($element, $originalElement, $sendMail, $message);
            } else {
                $contrib = $this->interactionService->createContribution($element, $message, UserInteractionType::PendingEdit);
                $element = $this->pendingService->savePendingModification($element, $originalElement, $contrib);
            }
        } else {
            // Add
            if ($isAllowedDirectModeration) {
                $this->elementActionService->add($element, $message);
            } else {
                $this->interactionService->createContribution($element, $message, UserInteractionType::PendingAdd);
                $this->pendingService->createPending($element);
            }
        }
        
        return $element;
    }

    // Wait for the message to be persisted
    public function afterAdd($element, $request)
    {
        // Do not send for pending elements
        if ($element->getStatus() > 0) {
            $sendMail = $request ? $request->request->get('send_mail') : false;
            $message = $request ? $request->request->get('admin-message') : null;
            $this->elementActionService->afterAdd($element, $sendMail, $message);
        }
    }

    // when user only make a minor modification, we don't want to go through moderation
    // Here is a function to detect minor changes
    private function isMinorModification($element, $originalElement)
    {
        $changeset = $element->getChangeSet($this->dm, $originalElement);
        $nonImportantAttributes = ['geo', 'openHours'];
        foreach ($changeset as $attribute => $values) {
            if (in_array($attribute, $nonImportantAttributes) 
                || strpos($attribute, 'Json') !== false
                || startsWith($attribute, 'osm_')) {
                unset($changeset[$attribute]);
            }
        }
        return 0 == count($changeset);
    }

    private function updateOptionsValues($element, $request)
    {
        $optionValuesString = $request->get('options-values');
        $optionValues = json_decode($optionValuesString, true);

        $element->resetOptionsValues();
        
        foreach ($optionValues as $optionValue) {
            $new_optionValue = new OptionValue();
            $new_optionValue->setOptionId($optionValue['id']);
            $new_optionValue->setIndex($optionValue['index']);
            $new_optionValue->setDescription($optionValue['description']);
            $element->addOptionValue($new_optionValue);
        }
    }

    private function updateCustomData($element, $request)
    {
        $data = $request->get('data');
        if (!$data) $data = [];
        // For some fields, like elements type, we store the data stringified in a data-json input
        if ($request->get('data-json')) {
            foreach ($request->get('data-json') as $key => $value) {
                $data[$key] = json_decode($value);
            }
        }
        $element->setCustomData($data);
    }

    private function updateOwner($element, $request, $userEmail)
    {
        if ($request->get('owning')) {
            $element->setUserOwnerEmail($userEmail);
        } else {
            $element->setUserOwnerEmail(null);
        }
    }
}
